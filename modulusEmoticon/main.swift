//
//  main.swift
//  modulusEmoticon
//
//  Created by Yefga Torra Prima on 03/07/18.
//  Copyright © 2018 Yefga Torra Prima. All rights reserved.
//

import Foundation


var max = 200


for x in 1...max {
    
    var count = 0

    for y in 2...max-1 {
        if Float(x) / Float(y) == Float(x/y) {
            count += 1
        }
    }


    if count == 1 && x % 2 == 0 {
        print("\(x) 🤩 😵 must be Prime")
    } else if count == 1 && x % 2 == 1 {
        print("\(x) 😎 😵 must be Prime")
    } else if x % 2 == 0 {
        print("\(x) 🤩 means Even")
    } else {
        print("\(x) 😎 means Odd")
    }

}
